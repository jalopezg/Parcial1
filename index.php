<?php ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Parcial</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css">
</head>
<body>
	<form action="bd.php" method="post">

		<h1 center>Parcial - Consulta e ingreso de datos</h1>
		<div class="form-floating mb-3">
   <input type="text" class="form-control"  placeholder="nombre" name="nombre">
    <label for="floatingInput">Nombre</label>
</div>
<div class="form-floating mb-3">
   <input type="text" class="form-control"  placeholder="apellido" name="apellido">
    <label for="floatingInput">Apellido</label>
</div>
<div class="form-floating mb-3">
   <input type="text" class="form-control"  placeholder="fecha_nacimiento" name="fecha_nacimiento">
    <label for="floatingInput">Fecha de nacimiento</label>
</div>
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="submit" class="btn btn-primary">Añadir estudiante</button>
  </div>

	</form>
</body>
</html>
